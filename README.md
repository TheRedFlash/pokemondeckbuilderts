## Pokemon Deckbuilding Game Tournament Software

Welcome to the repository for the PDBG Tournament Software!

The Pokemon Deckbuilding Game is a fan project created by a guy called Cory aka 3vo. This repository is for the tournament system being developed by myself, Chris aka TheRedFlash.

This is completly open-source so if you want to use the software for yourself or maybe contribute then feel free to clone it and submit a pull request! I'm mostly a beginner
when it comes to software development as I come from a games design background, so if you see anything that could be done better please feel free to submit it or message me on Discord.

If you want to play the game/get involved with the community feel free to join the Discord below!

https://discord.gg/PrYZPJ2

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.